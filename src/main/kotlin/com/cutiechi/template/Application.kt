package com.cutiechi.template

/**
 * Gradle kotlin DSL kotlin application template Main Class
 *
 * @author Cutie Chi
 */
class Application

/**
 * Main method
 */
fun main () = print("Hello Kotlin!")
