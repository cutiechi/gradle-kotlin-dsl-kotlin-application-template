/**
 * 版本常量类
 *
 * @author Cutie Chi
 */
object Versions {

    /**
     * kotlin
     */
    const val kotlin = "1.3.21"

    /**
     * lombok
     */
    const val lombok = "1.18.6"

    /**
     * junit jupiter engine
     */
    const val junitJupiterEngine = "5.4.2"
}
