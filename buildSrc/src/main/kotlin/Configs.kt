object Configs {

    /**
     * root project name
     */
    const val rootProjectName = "kotlin-application-template"

    /**
     * main class name
     */
    const val mainClassName = "com.cutiechi.template.Application"
}
