/**
 * Gradle 依赖常量类
 *
 * @author Cutie Chi
 */
object Dependencies {

    /**
     * kotlin JDK 8 standard library
     */
    const val kotlinStdlibJdk8 = "stdlib-jdk8"

    /**
     * lombok
     */
    const val lombok = "org.projectlombok:lombok:${Versions.lombok}"

    /**
     * junit jupiter engine
     */
    const val junitJupiterEngine = "org.junit.jupiter:junit-jupiter-engine:${Versions.junitJupiterEngine}"
}
