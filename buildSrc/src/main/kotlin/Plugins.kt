/**
 * Gradle 插件常量类
 *
 * @author Cutie Chi
 */
object Plugins {

    /**
     * kotlin JVM
     */
    const val kotlinJvm = "org.jetbrains.kotlin.jvm"
}
