plugins {

    id(Plugins.kotlinJvm).version(Versions.kotlin)

    application
}

repositories {
    
    mavenCentral()
}

dependencies {

    implementation(kotlin(Dependencies.kotlinStdlibJdk8))

    annotationProcessor(Dependencies.lombok)

    testImplementation(Dependencies.junitJupiterEngine)

    testAnnotationProcessor(Dependencies.lombok)
}

application {

    mainClassName = Configs.mainClassName
}
